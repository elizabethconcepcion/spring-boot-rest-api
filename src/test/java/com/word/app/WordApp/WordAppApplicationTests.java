package com.word.app.WordApp;

import com.word.app.WordApp.Controller.MeaningSearchController;
import com.word.app.WordApp.Controller.model.Word;
import com.word.app.WordApp.Repository.WordRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.net.MalformedURLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WordAppApplicationTests {
    @Autowired
    WordRepo wordRepo;
    Long idForTest=1L;

    @Test

    public void testSaveWord() throws MalformedURLException {
        Word word = new Word();
        word.setWord("Tea");
        MeaningSearchController meaningSearchController = new MeaningSearchController(word.getWord());
        word.setMeaning(meaningSearchController.getMeaning());
        wordRepo.save(word);
        assertNotNull(wordRepo.findById(word.getId()).get());

    }
    @Test
    public void getWords(){
        List<Word> words = wordRepo.findAll();
        assertThat(words).size().isGreaterThan(0);

    }
    @Test
    public void getWord(){
        Word word = wordRepo.findById(idForTest).get();
        assertEquals("Art", word.getWord());

    }
   @Test
   public void testUpdateWord() throws MalformedURLException {
       Word word = wordRepo.findById(idForTest).get();
       word.setWord("Art");
       MeaningSearchController meaningSearchController = new MeaningSearchController(word.getWord());
       word.setMeaning(meaningSearchController.getMeaning());
       wordRepo.save(word);
       assertNotEquals("Computer", wordRepo.findById(idForTest).get().getWord());

   }
    @Test
    public void TestDeleteWord(){
        wordRepo.deleteById(idForTest);
        assertThat(wordRepo.existsById(idForTest)).isFalse();

    }
}
