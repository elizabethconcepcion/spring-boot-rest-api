package com.word.app.WordApp.Repository;

import com.word.app.WordApp.Controller.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WordRepo extends JpaRepository<Word, Long> {

}
