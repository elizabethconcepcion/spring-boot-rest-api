package com.word.app.WordApp.Controller;

import com.word.app.WordApp.Controller.model.Word;
import com.word.app.WordApp.Repository.WordRepo;
import com.word.app.WordApp.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;

@RestController
public class WordController {

    @Autowired
    private WordRepo wordRepo;
    

    @GetMapping(value="/words")
    public List<Word> getWords(){
        return wordRepo.findAll();

    }
    @GetMapping(value="/{id}")
    public Word getWordById(@PathVariable (value="id") Long id){
        return this.wordRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id :" + id));
    }

    @PostMapping(value="/save")
    public Word  saveWord(@RequestBody Word word) throws MalformedURLException, FileNotFoundException {
        Word newWord = new Word();
        Optional<String> nullValue = Optional.ofNullable(null);
        String validWord = word.getWord();
        if(validWord.isEmpty()) {
            validWord = nullValue.orElseThrow(() -> new FileNotFoundException("You must enter a valid record"));
        }
        newWord.setWord(word.getWord());
        MeaningSearchController meaningSearchController = new MeaningSearchController(word.getWord());
        newWord.setMeaning(meaningSearchController.getMeaning());
        return this.wordRepo.save(newWord);
    }

    @PutMapping(value="/update/{id}")
    public String  updateWord(@PathVariable Long id, @RequestBody Word word) throws MalformedURLException, FileNotFoundException {
    Word updateWord = wordRepo.findById(id).get();
    Optional<String> nullValue = Optional.ofNullable(null);
    String validWord = word.getWord();
    if(validWord.isEmpty()) {
        validWord = nullValue.orElseThrow(() -> new ResourceNotFoundException("You must enter a valid record"));
    }
    updateWord.setWord(word.getWord());
    MeaningSearchController meaningSearchController = new MeaningSearchController(word.getWord());
    updateWord.setMeaning(meaningSearchController.getMeaning().toString());


        wordRepo.save(updateWord);
        return "Updated ...";
    }

    @DeleteMapping(value="/delete/{id}")
    public String  deleteWord(@PathVariable Long id){
        Word deleteWord = wordRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("You cannot delete word with id :" + id +" because do not exist"));;
        wordRepo.delete(deleteWord);
        return "Deleted word with ID: " +id;
    }



}
